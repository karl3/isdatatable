(function ($) {
  /*
   ======== A Handy Little QUnit Reference ========
   http://api.qunitjs.com/

   Test methods:
   module(name, {[setup][ ,teardown]})
   test(name, callback)
   expect(numberOfAssertions)
   stop(increment)
   start(decrement)
   Test assertions:
   ok(value, [message])
   equal(actual, expected, [message])
   notEqual(actual, expected, [message])
   deepEqual(actual, expected, [message])
   notDeepEqual(actual, expected, [message])
   strictEqual(actual, expected, [message])
   notStrictEqual(actual, expected, [message])
   throws(block, [expected], [message])
   */

  test('Util #0, rowCount', function () {
    equal($('#util-0').rowCount(), 3, 'table should have 3 rows');
  });

  test('Util #2, Table hasAttr border', function () {
    ok($('#util-2').hasAttr('border') === true);
  });

  test('Util #2.1, Table does not have attribute, bgcolor', function () {
    ok($('#util-2').hasAttr('bgcolor') === false);
  });

  test('Util #3, cellCount', function () {
    equal($('#util-3').cellCount(), 3, 'table should have 3 cells');
  });

  test('Util #5, colCount', function () {
    equal($('#util-5').colCount(), 5, 'table should have 5 cols');
  });

  test('Util #5-1, colCount', function () {
    equal($('#util-5-1').colCount(), 3, 'table should have 3 cols');
  });

  test('Util #6, getStyleObject', function () {
    ok(typeof $('#test_0').getStyleObject() === 'object');
  });

  test('TEST #0 role=presentation is layout table', function () {
    ok($('#test_0').isDataTable() === false);
  });

  test('TEST #1 contenteditable = true; is data table', function () {
    ok($('#test_1').isDataTable() === true);
  });

  test('TEST #2 contenteditable = inherit; is data table', function () {
    ok($('#test_2').isDataTable() === true);
  });

  test('TEST #3 role=grid; is datatable', function () {
    ok($('#test_3').isDataTable() === true);
  });

  test('TEST #4 role=treegrid; is data table', function () {
    ok($('#test_4').isDataTable() === true);
  });

  test('TEST #5 Table having datatable="0" attribute is layout table', function () {
    ok($('#test_5').isDataTable() === false);
  });

  test('TEST #5.1 Table having datatable="1" attribute is data table', function () {
    ok($('#test_5-1').isDataTable() === true);
  });

  test('TEST #6 Table having summary attribute is data table', function () {
    ok($('#test_6').isDataTable() === true);
  });

  test('TEST #7 Table having caption element is data table', function () {
    ok($('#test_7').isDataTable() === true);
  });

  test('TEST #8 Table having col element is data table', function () {
    ok($('#test_8').isDataTable() === true);
  });

  test('TEST #9 Table having colgroup elements is data table', function () {
    ok($('#test_9').isDataTable() === true);
  });

  test('TEST #10 Table having tfoot elements is data table', function () {
    ok($('#test_10').isDataTable() === true);
  });

  test('TEST #11 Table having thead elements is data table', function () {
    ok($('#test_11').isDataTable() === true);
  });

  test('TEST #12 Table having th elements is data table', function () {
    ok($('#test_12').isDataTable() === true);
  });

  test('TEST #13 table having th/ td elements with headers attributes is data table', function () {
    ok($('#test_13').isDataTable() === true);
  });

  test('TEST #14 table having th/ td elements with scope attributes is data table', function () {
    ok($('#test_14').isDataTable() === true);
  });

  test('TEST #15 table having th/ td elements with abbr attributes is data table', function () {
    ok($('#test_15').isDataTable() === true);
  });

  test('TEST #16 Table having nested table is layout table', function () {
    ok($('#test_16').isDataTable() === false);
  });

  test('TEST #17 Table having only one row is layout table', function () {
    ok($('#test_17').isDataTable() === false);
  });

  test('TEST #17.1.0 Table row count', function () {
    equal($('#test_17-1').rowCount(), 10, 'Table should have 10 rows');
  });

  test('TEST #17.1 Table having more than one row is data table', function () {
    ok($('#test_17-1').isDataTable() === true);
  });

  test('TEST #18 Table having only one column is layout table', function () {
    ok($('#test_18').isDataTable() === false);
  });

  test('TEST #19 Table having many columns (>= 5) is data table', function () {
    ok($('#test_19').isDataTable() === true);
  });

  test('TEST #20 Table having borders around cells is data table', function () {
    ok($('#test_20').isDataTable() === true);
  });

  //TEST #21 was removed/ replaced with #32

  test('TEST #22 Table having many rows (>= 20) is data table', function () {
    ok($('#test_22').isDataTable() === true);
  });

  /**
   * NOTE this test will fail if you run tests from Grunt because it tests against
   * document.width()
   */
  test('TEST #23 Wide table (more than 95% of the document width) is layout table', function () {
    ok($('#test_23').isDataTable() === false);
  });

  test('TEST #24 Table having small amount of cells (<= 10) is layout table', function () {
    ok($('#test_24').isDataTable() === false);
  });

  test('TEST #25 Table containing embed is layout table', function () {
    ok($('#test_25').isDataTable() === false);
  });

  test('TEST #26 Table containing object is layout table', function () {
    ok($('#test_26').isDataTable() === false);
  });

  test('TEST #27 Table containing applet is layout table', function () {
    ok($('#test_27').isDataTable() === false);
  });

  test('TEST #28 Table containing iframe is layout table', function () {
    ok($('#test_28').isDataTable() === false);
  });

  test('TEST #30 Table containing children with grid-related roles is data table', function () {
    ok($('#test_30-1').isDataTable() === true);
    ok($('#test_30-2').isDataTable() === true);
    ok($('#test_30-3').isDataTable() === true);
    ok($('#test_30-4').isDataTable() === true);
    ok($('#test_30-5').isDataTable() === true);
    ok($('#test_30-6').isDataTable() === true);
  });

  test('TEST #31 Table containing many columns is a data table', function () {
    ok($('#test_31').isDataTable() === false);
  });

  test('TEST #32 Table having different colored cells is a data table', function () {
    ok($('#test_32').isDataTable() === true);
    ok($('#test_32-1').isDataTable() === true);
  });

}(jQuery));