/*
 * isDataTable
 * Plugin to determine whether or not a table is (and therefore should be treated as) a data table
 * It is designed to return early.
 *
 * The table is subjected to a number of test criteria, in order of importance, to determine whether or not
 * it should be treated as a data table.  This code closely approximates (or is identical to) the criteria
 * used by Chrome and Firefox.
 *
 * Copyright (c) 2014 Karl Groves, Tenon.io
 * Licensed under the MIT license.
 */

(function ($) {

  /**
   * returns an object containing key:val pairs where
   *  key = array key (allowing for associative arrays)
   *  val = the # of occurances of the value
   * @param arr
   * @returns {{}}
   */
  var arrayCount = function (arr) {
    var obj = { };
    for (var i = 0, j = arr.length; i < j; i++) {
      if (obj[arr[i]]) {
        obj[arr[i]]++;
      }
      else {
        obj[arr[i]] = 1;
      }
    }
    return obj;
  };

  /**
   * Creates an array of all attributes on a specified element
   * @returns {{}}
   */
  $.fn.getAttributes = function () {
    var attributes = {};

    if (this.length) {
      $.each(this[0].attributes, function (index, attr) {
        attributes[ attr.name ] = attr.value;
      });
    }
    return attributes;
  };

  /**
   * Determines whether or not a specified attribute exists on the element
   * @param name
   * @returns {boolean}
   */
  $.fn.hasAttr = function (name) {
    return this.attr(name) !== undefined;
  };

  /**
   * Counts the number of TR elements within a selector
   * @returns {Number}
   */
  $.fn.rowCount = function () {
    return this.find('tr').length;
  };

  /**
   * Counts the number of TD or TH elements within a selector. Useful for counting cells in a row or in the entire table
   * @returns {Number}
   */
  $.fn.cellCount = function () {
    return this.find('td, th').length;
  };

  /**
   * Utility function to get the percentage of cells that have CSS border
   * @returns {number}
   */
  $.fn.countBordersPct = function () {
    var self = $(this),
      childCells = self.find('td, th'),
      childCellCount = childCells.length,
      cellBorderCount = 0;

    childCells.each(function () {
      if (self.css('border-width') !== '0px') {
        cellBorderCount++;
      }
    });

    return (cellBorderCount / childCellCount) * 100;
  };

  /**
   * Counts the number of columns in a table
   * Since there's no such thing as cols.length, this method is
   * largely paving the cow-path by averaging the # of cells in each row.
   * @returns {number}
   */
  $.fn.colCount = function () {
    var self = $(this),
      rows = self.find('tr'),
      sum = 0,
      i,
      rowCount;

    rowCount = rows.length;

    for (i = 0; i < rowCount; i++) {
      sum += parseInt(rows[i].cells.length, 10); //don't forget to add the base
    }

    return Math.round(sum / rowCount);

  };

  /**
   *
   * @param maxColors
   * @param maxDiffs
   * @returns {boolean}
   */
  $.fn.cellColorDiffs = function (maxColors, maxDiffs) {
    var self = $(this),
      cells = self.find('td, th'),
      colors = [],
      colorObj;

    cells.each(function () {
      var c = $(this),
        cStyle = c.getStyleObject();

      colors.push(cStyle.backgroundColor);
    });

    colorObj = arrayCount(colors);

    // after a sanity check, we look at the percentage difference
    if (colorObj.length > 1) {
      if (colorObj.length > maxColors) {
        return true;
      }
      $.each(colorObj, function (key, value) {
        if (((value / colorObj.length) * 100) > maxDiffs) {
          return true;
        }
      });
    }
    return false;
  };

  /**
   * gets all calculated styles for an object.
   * Example:
   *    var fooProps = $('#foo').getStyleObject();
   * You can then access a specific CSS property as:
   *    fooProps.backgroundColor;
   * @returns {*}
   */
  $.fn.getStyleObject = function () {
    var dom = this.get(0);
    var style;
    var returns = {};
    var prop;
    if (window.getComputedStyle) {
      var camelize = function (a, b) {
        return b.toUpperCase();
      };
      style = window.getComputedStyle(dom, null);
      for (var i = 0, l = style.length; i < l; i++) {
        prop = style[i];
        var camel = prop.replace(/\-([a-z])/g, camelize);
        var val = style.getPropertyValue(prop);
        returns[camel] = val;
      }
      return returns;
    }
    if (style = dom.currentStyle) {
      for (prop in style) {
        returns[prop] = style[prop];
      }
      return returns;
    }
    return this.css();
  };

  /**
   * This is the plugin itself.
   * @param options
   * @returns {boolean}
   */
  $.fn.isDataTable = function (options) {
    var self = $(this),
      settings,
      defaults;

    if (typeof options === 'undefined') {
      options = false;
    }

    defaults = {
      // number of columns after which we consider it a data table
      numDTColumns: 5,

      // % of cells in the table with css border needed to consider it a data table
      pctCellsWBorder: 33,

      // number of rows after which we consider it a data table
      numDTRows: 20,

      // maximum % of page width before we consider it a layout table
      maxWidthLT: 95,

      // # of cells below which we consider it a layout table
      minCellsLT: 10,

      // max number of different RGB colors in a table after which we consider it a layout table
      maxCellColors: 3,

      // max percentage of in a table after which we consider it a layout table
      maxColorDiffs: 30

    };

    // merge the options and defaults to assemble our settings
    settings = {
      numDTColumns: options.numDTColumns || defaults.numDTColumns,
      pctCellsWBorder: options.pctCellsWBorder || defaults.pctCellsWBorder,
      numDTRows: options.numDTRows || defaults.numDTRows,
      maxWidthLT: options.maxWidthLT || defaults.maxWidthLT,
      minCellsLT: options.minCellsLT || defaults.minCellsLT,
      maxCellColors: options.maxCellColors || defaults.maxCellColors,
      maxColorDiffs: options.maxColorDiffs || defaults.maxColorDiffs
    };


    // first, a sanity check: is this actually a table we're dealing with?
    if (self.is('table') === false) {
      return false;
    }

    //Table inside editable area is data table always since the table structure is crucial for table editing
    else if (self.closest("*[contenteditable='true'], *[contenteditable='inherit'] ").length > 0) {
      return true;
    }

    // table with presentation role is layout table
    else if (self.is("table[role='presentation']")) {
      return false;
    }

    //Table having ARIA table related role is data table (like ARIA grid or treegrid)
    else if ((self.is("table[role='grid']")) || (self.is("table[role='treegrid']"))) {
      return true;
    }

    //  if TD, TH children have role of gridcell, rowheader, columnheader it is a data table
    var roleChildren = $('th, td').filter("[role='gridcell'], [role='rowheader'], [role='columnheader']");
    if ((self.find(roleChildren).length > 0)) {
      return true;
    }

    //  if TR has role of row it is a data table
    if ((self.find("tr[role='row']").length > 0)) {
      return true;
    }

    // if any child has role of rowgroup it is a data table
    if ((self.find('*[role="rowgroup"]').length > 0)) {
      return true;
    }

    //Table having or its descendants having any other role, its a layout table
    else if ((self.is('[role]')) ||

      (self.find('td, th').hasAttr('role')) ||

      (self.find('thead, tbody, tfoot, tr').hasAttr('role'))) {
      return false;
    }

    //Table having datatable="0" attribute is layout table
    // this is a weird artifact of an old JAWS thing
    else if (self.is("table[datatable='0']")) {
      return false;
    }

    //Table having datatable="1" attribute is data table
    // this is a weird artifact of an old JAWS thing
    else if (self.is("table[datatable='1']")) {
      return true;
    }

    //Table having summary attribute or other legitimate data table structures is data table;
    else if (
      (self.hasAttr('summary')) ||

        (self.hasAttr('rules')) ||

        (self.find('caption').length > 0) ||

        (self.find('col, colgroup').length > 0) ||

        (self.find('tfoot, thead').length > 0) ||

        (self.find('th').length > 0) ||

        (self.find('td[headers], td[scope], td[abbr], td[axis]').length > 0)
      ) {
      return true;
    }

    //Table having nested table is layout table
    else if (self.find('table').length > 0) {
      return false;
    }

    //Table having only one row or column is layout table
    else if ((self.rowCount() === 1) || (self.cellCount() === 1)) {
      return false;
    }

    //Table having many columns (>= settings.numDTColumns) is data table
    else if (self.colCount() >= settings.numDTColumns) {
      return true;
    }

    //Table having borders around cells is data table
    else if ((self.hasAttr('border')) && ((self.attr('border') !== '0'))) {
      return true;
    }

    //If > 1/3 of the cells have CSS borders, it is a data table
    else if (self.countBordersPct() > settings.pctCellsWBorder) {
      return true;
    }

    //Table having differently colored cells is data table
    else if (self.cellColorDiffs(settings.maxCellColors, settings.maxColorDiffs) !== false) {
      return true;
    }

    //Table having many rows (>= settings.numDTRows) is data table
    else if (self.rowCount() >= settings.numDTRows) {
      return true;
    }

    //Wide table (more than settings.maxWidthLT of the document width) is layout table
    else if (((self.width() / $(document).width()) * 100) > settings.maxWidthLT) {
      return false;
    }

    //Table having small amount of cells (<= settings.minCellsLT) is layout table
    else if (self.find('td').length <= settings.minCellsLT) {
      return false;
    }

    //Table containing embed, object, applet or iframe elements (typical advertisements elements) is layout table
    else if (self.find('embed, object, applet, iframe').length > 0) {
      return false;
    }

    //If we've gotten this far, it is a data table
    return true;

  };
}(jQuery)
  )
;