# isDataTable

plugin to determine if a table is a data table, based on the same rules that are used by browsers.

## Getting Started

Download the [production version][min] or the [development version][max].

[min]: https://raw.github.com/karlgroves/jquery-isdatatable/master/dist/jquery.isdatatable.min.js
[max]: https://raw.github.com/karlgroves/jquery-isdatatable/master/dist/jquery.isdatatable.js

In your web page:

```html
<script src="jquery.js"></script>
<script src="dist/isdatatable.min.js"></script>
<script>
jQuery(function($) {
  // would return boolean true or false
  $('#foo').isDataTable(); 
});
</script>
```